\ParaView can be customized in a number of ways to tailor it to your
preferences and needs. Customization options include setting general
application behavior, customizing default property values used for filters,
representations, and views, and customizing aspects of the \paraview client.
This chapter describes the different ways to customize \ParaView.

\section{Settings}
\label{sec:Settings}
\keyword{Settings}
As with any large application, \paraview provides mechanisms to customize some
of its application behavior. These are referred to as \keyterm{application
settings}. or just {settings}. Such settings can be changed using the \ui{Settings} dialog,
which is accessed from  the \menu{Edit > Settings} menu (\menu{ParaView >
Preferences} on the Mac). We have seen parts of this dialog earlier, e.g., in
Sections~\ref{sec:PropertiesPanelLayoutSettings},
\ref{sec:BasicRenderingSettings}, and \ref{sec:ParallelRenderParameters}. In
this section, we will take a closer look at some of the other options available
in this dialog.

The \ui{Settings} dialog is split into several tabs. The \ui{General} tab
consolidates most of the miscellaneous settings. The \ui{Camera} tab enables you
to change the mouse interaction mappings for the \ui{Render View} and similar
views. The \ui{Render View} tab, which we saw earlier in
Sections~\ref{sec:BasicRenderingSettings}
and~\ref{sec:ParallelRenderParameters}, provides options in regards to rendering in
\ui{Render View} and similar views. The \ui{Color Palette} tab is used to change
the active color palette.

Using this dialog is not much different than the \ui{Properties} panel. You have
the \ui{Search} box at the top, which allows you to search properties matching
the input text (Section~\ref{sec:PropertiesPanel:SearchBox}). The
\icon{Images/pqAdvanced26.png} button can be used to toggle between default and
advanced modes.

To apply the changes made to any of the settings, use the \ui{Apply} or \ui{OK}
buttons. \ui{OK} will apply the changes and close the dialog, while \ui{Cancel}
will reject any changes made and close the dialog. Any changes made to
the options in this dialog are persistent across sessions. That is, the next time you
launch \paraview, you'll still be using the same settings chosen earlier. To
revert to the default, use the \ui{Restore Defaults} button. You can also manually
edit the setting file as in Section~\ref{sec:ConfiguringDefaultSettingsJSON}.
Furthermore, site maintainers can provide site-wide defaults for these, as is
explained in Section~\ref{sec:ConfiguringDefaultSettingsSiteWide}.

Next, we will see some of the important options available. Those that are only
available in the advanced mode are indicated as such using the
\icon{Images/pqAdvanced26.png} icon. You will either need to toggle on the
advanced options with the \icon{Images/pqAdvanced26.png} button or search for the
option using the \ui{Search} box.

\subsection{\texttt{General} settings}
\label{sec:GeneralSettings}
\keyword{General Settings}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/SettingsDialog.png}
\caption{\ui{Settings} dialog in \paraview showing the \ui{General} settings tab.}
\label{fig:SettingsDialog}
\end{center}
\end{figure}

\begin{description}
\item[\ui{General Options}]~
  \begin{itemize}
  \item \ui{Show Welcome Dialog}: Uncheck this to not show the welcome screen at
  application startup. You will need to restart \paraview to see the effect.
  \icon{Images/pqAdvanced26.png}
  \item \ui{Show Save State On Exit}: When this is checked \paraview will prompt you
  to save a state file when you exit the application. \icon{Images/pqAdvanced26.png}
  \item \ui{Crash Recovery}: When this is checked, \paraview will intermittently
  save a backup state file as you make changes in the visualization pipeline.
  If \paraview crashes for some reason, then when you relaunch \paraview, it
  will provide you with a choice to load the backup state saved before the crash
  occurred. This is not $100\%$ reliable, but some users may find it useful to
  avoid losing their visualization state due to a crash. \icon{Images/pqAdvanced26.png}
  \item \ui{Force Single Column Menus}: On platforms that support multicolumn menus,
  force all menu items to appear in a single scrollable column. This is useful to
  ensure all menu items are selectable on low-resolution screens.
  \end{itemize}

\item[\ui{GUI Font}]~
  \begin{itemize}
  \item \ui{Override Font}: When checked, use a custom font size for the user interface.
  This overrides the system default font size.
  \item \ui{Font Size}: The size of the font to use for UI elements.
  \end{itemize}

\item[\ui{View Options}]~
  \begin{itemize}
  \item \ui{Default View Type}: When \paraview starts up, it
  creates \ui{Render View} by default. You can use this option to change the
  type of the view that is created by default, instead of the \ui{Render View}.
  You can even pick \texttt{None} if you don't want to create any view by
  default.
  \icon{Images/pqAdvanced26.png}
  \end{itemize}

\item[\ui{Properties Panel Options}]~
  \begin{itemize}
  \item \ui{Auto Apply}: When checked, the \ui{Properties} panel will
  automatically apply any changes you make to the properties without requiring you
  to click the \ui{Apply} button. The same setting can also be toggled using the
  \icon{Images/AutoApplyIcon.png} button in the \ui{Main Controls} toolbar.
  \item \ui{Auto Apply Active Only}: This limits the auto-applying to the properties
  on the active source alone.
  \icon{Images/pqAdvanced26.png}
  \item \ui{Properties Panel Mode}: This allows you to split the \ui{Properties}
  panel into separate panels as described in
  Section~\ref{sec:PropertiesPanelLayoutSettings}.
  \icon{Images/pqAdvanced26.png}
  \end{itemize}

\item[\ui{Data Processing Options}]~
  \begin{itemize}
  \item \ui{Auto Convert Properties}: Several filters only work on one type of
  array, e.g., point data arrays or cell data arrays. Before using such filters,
  you are expected to apply the \ui{Point Data To Cell Data} or \ui{Cell Data To
  Point Data} filters. To avoid having to add these filters explicitly, you can
  check this checkbox. When checked, \ParaView will automatically convert data
  arrays as needed by filters, including converting cell array to point arrays
  and vice-versa, as well as extracting a single component from a
  multi-component array.
  \end{itemize}

\item[\ui{Color/Opacity Map Range Options}]~
  \begin{itemize}
  \item \ui{Transfer Function Reset Mode}: This setting controls the initial
  settings for how \ParaView
  will reset the ranges for color and opacity maps (or transfer functions). This
  sets the initial value of the \ui{Automatic Rescale Range Mode} for newly created
  color/opacity maps (Section~\ref{sec:ColorMapping:MappingData}). This setting can
  be changed on a per-color map basis after the color map has been created.

  \item \ui{Scalar Bar Mode}: This settings controls how \paraview
  manages showing the color legend (or scalar bar) in \ui{Render View} and
  similar views.
  \end{itemize}

\item[\ui{Default Time Step}]~

  Whenever a dataset with timesteps is opened, this setting controls how
  \paraview will update the current time shown by the application. You can choose
  between \texttt{Leave current time unchanged, if possible}, \texttt{Go to
  first timestep}, and \texttt{Go to last timestep}.

\item[\ui{Animation}]~
  \begin{itemize}
  \item \ui{Cache Geometry For Animation}: This enables caching of geometry when
  playing animations to attempt to speed up animation playback in a loop. When
  caching is enabled, data ranges reported by the \ui{Information} panel and
  others can be incorrect, since the pipeline may not have updated.
  \icon{Images/pqAdvanced26.png}
  \item \ui{Animation Geometry Cache Limit}: When animation caching is enabled,
  this setting controls how much geometry (in kilobytes) can be cached by any
  rank. As soon as a rank's cache size reaches this limit, \ParaView will no
  longer cache the remaining timesteps.
  \icon{Images/pqAdvanced26.png}
  \item \ui{Animation Time Notation}: Sets the display notation for the time in
  the annotation toolbar. Options are \ui{Mixed}, \ui{Scientific}, and \ui{Fixed}.
  \item \ui{Animation Time Precision}: Sets the number of digits displayed in the
  time in the animation toolbar.
  \end{itemize}

\item[\ui{Maximum Number of Data Representation Labels}]~

  When a selection is labeled by data attributes this is the maximum number of labels
  to use.  When the number of points/cells to label is above this value then a subset
  of this many will be labeled instead. Too many overlapping labels becomes illegible,
  so this is set to 100 by default.

%\item[\ui{Miscellaneous}]~
%  \begin{itemize}
%  \item \ui{Inherit Representation Properties}: Set whether representations try
%  to maintain properties from an input representation, if present. For example,
%  if you \ui{Transform} the representation for a source, then any filter that
%  you connect to it and show in the same view will also get the same
%  transformation.
%  \icon{Images/pqAdvanced26.png}
%  \end{itemize}
\end{description}

\subsection{\texttt{Camera} settings}
\keyword{Camera Settings}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/SettingsDialogCamera.png}
\caption{\ui{Settings} dialog in \paraview showing the \ui{Camera} settings tab.}
\label{fig:SettingsDialogCamera}
\end{center}
\end{figure}

This tab allows you to control how you can interact in \ui{Render View} and
similar views. Basically, you are setting up a mapping between each of the mouse
buttons and keyboard modifiers, and the available interaction types including
\texttt{Rotate}, \texttt{Pan}, \texttt{Zoom}, etc. The dialog allows you to set
the interaction mapping separately for 3D and 2D interaction modes
(see Section~\ref{sec:RenderView:Interactions}).

\subsection{\texttt{Render View} settings}

Refer to Sections \ref{sec:BasicRenderingSettings} and
\ref{sec:ParallelRenderParameters} for various options available on the
\ui{Render View} tab.

\subsection{\texttt{Color Palette}}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/SettingsDialogColorPalette.png}
\caption{\ui{Settings} dialog in \paraview showing the \ui{Color Palette} settings tab.}
\label{fig:SettingsDialogColorPalette}
\end{center}
\end{figure}

The \ui{Color Palette} tab (Figure~\ref{fig:SettingsDialogColorPalette})
allows you to change the colors in the active color
palette. The tab lists the available color categories \ui{Surface},
\ui{Foreground}, \ui{Edges}, \ui{Background}, \ui{Text}, and \ui{Selection}. You
can manually set colors to use for each of these categories or load one of the
predefined palettes using the \ui{Load Palette} option. To understand
\keyterm{color palettes}, let's look at an example.

Let's start \paraview and split the active view to create two \ui{Render View}
instances side by side. You may want to start \paraview with the \texttt{-dr}
command line argument to stop any of your current settings from interfering
with this demo. Next, show \ui{Sphere} as \ui{Wireframe} in the view
on the left, and show \ui{Cone} as \ui{Surface} in the view on the right.
Also, turn on \ui{Cube Axis} for \ui{Cone}. You will see something like
Figure~\ref{fig:ColorPaletteDemo} (left).

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.45\linewidth]{Images/ColorPaletteDemo1.png}
\includegraphics[width=0.45\linewidth]{Images/ColorPaletteDemo2.png}
\caption{The effect of loading the \ui{Print} color palette as
the active palette. The left is the original visualization and the right shows the
result after loading the \ui{Print} palette.}
\label{fig:ColorPaletteDemo}
\end{center}
\end{figure}

Now let's say you want to generate an image for printing. Typically, for
printing, you'd want the background color to be white, while the wireframes and
annotations to be colored black. To do that, one way is to go change each of the
colors for each each of the views, displays and cube-axes. You can imagine how
tedious that will get especially with larger pipelines. Alternatively, using the
\ui{Settings} dialog, change the active color palette to \ui{Print} as shown in
Figure~\ref{fig:SettingsDialogColorPalette} and then click \ui{OK} or \ui{Apply}.
The visualization will immediately change to something like
Figure~\ref{fig:ColorPaletteDemo} (right).

Essentially, \ParaView allows you to \emph{link} any color property to one of
the color categories. When the color palette is changed, any color property
linked to a palette category will also be automatically updated to match the category color.
Figure~\ref{fig:LinkToColorPalette} shows how to link a color
property to a color palette category in the \ui{Properties} panel. Use the tiny
drop-down menu marker to make the menu pop up that shows the color palette categories.
Select any one of them to link that property with the category. The link is
automatically severed if you manually change the color by simply clicking on the
button.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/LinkToColorPalette.png}
\caption{Popup menu allows you to link a color property to a color palette
category in the \ui{Properties} panel.}
\label{fig:LinkToColorPalette}
\end{center}
\end{figure}

\section{Custom default settings}
\label{sec:CustomDefaultSettings}

The section describes how to specify custom default settings for the
properties of sources, readers, filters, representations, and views.
This can be used to specify, for example, the default background color
for new views, whether a gradient background should be used, the
resolution of a sphere source, which data arrays to load from a
particular file format, and the default setting for almost any other
object property.

The same custom defaults are used across all the \ParaView
executables. This means that custom defaults specified in the
\paraview executable are also used as defaults in
\pvpython and \pvbatch, which makes it easier
to set up a visualization with \paraview and use
\pvpython or \pvbatch to generate an animation
from time-series data, for example.

\subsection{Customizing defaults for properties}

The Properties panel in \paraview has three sections,
\ui{Properties}, \ui{Display}, and \ui{View}. Each section has two
buttons. These buttons are circled in red in
Figure~\ref{fig:SaveRestoreSettingsButtons}. The button with the disk
icon is used to save the current property values in that section that
have been applied with the \ui{Apply} button. Property values that
have been changed but not applied with the \ui{Apply} button will not
be saved as custom default settings.

The button with the circular arrow (or reload icon) is used to restore
any custom property settings for the object to \ParaView's application
defaults. Once you save the current property settings as defaults, those
values will be treated as the defaults from then on until you change
them to another value or reset them. The saved defaults are written to
a configuration file so that they are available when you close and
launch \ParaView again.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/SaveRestoreSettingsButtons.png}
\caption{Buttons for saving and restoring default property values in the \ui{Properties} panel.}
\label{fig:SaveRestoreSettingsButtons}
\end{center}
\end{figure}

You can undo your changes to the default property values by clicking
on the reload button. This will reset the current view property values
to \paraview's application defaults. To fully restore
\paraview's default values, you need to click the save
button again. If you don't, the restored default values will be
applied only to the current object, and new instances of that object
will have the custom default values that were saved the last time you
clicked the save button.

\subsection{Example: specifying a custom background color}

Suppose you want to change the default background color in the
\ui{Render View}. To do this, scroll down to the \ui{View} section of
the \ui{Properties} panel and click on the combo box that shows the
current background color. Select a new color, and click \ui{OK}. Next,
scroll up to the \ui{View (Render View)} section header and click on
the disk button to the right of the header. This will save the new
background color as the default background color for new views. To see
this, click on the \ui{+} sign next to the tab above the 3D view to
create a new layout. Click on the \ui{Render View} button. A new
render view will be created with the custom background color you just
saved as default.

\subsection{Configuring default settings with JSON}
\label{sec:ConfiguringDefaultSettingsJSON}

Custom default settings are stored in a text file in the \JSON
format. We recommend to use the user interface
in \paraview to set most default values, but it is
possible to set them by editing the \JSON settings file directly. It is
always a good idea to make a backup copy of a settings file prior to
manual editing.

The \ParaView executables read from and write to a file named
\directory{ParaView-UserSettings.json}, which is located in your home
directory on your computer. On Windows, this file is located at
\directory{\%APPDATA\%/ParaView/ParaView-UserSettings.json}, where the
\texttt{APPDATA} environment variable is usually something like
\directory{C:/Users/USERNAME/AppData/Roaming}, where \texttt{USERNAME} is
your login name. On Unix-like systems, it is located under
\directory{\textasciitilde/.config/ParaView/ParaView-UserSettings.json}. This file will
exist if you have made any default settings changes through the user
interface in the \paraview executable. Once set, these
default settings will be available in subsequent versions of \ParaView.

A simple example of a file that specifies custom default settings is
shown below:

\begin{python}
{
  "sources" : {
    "SphereSource" : {
      "Radius" : 3.5,
      "ThetaResolution" : 32
    },
    "CylinderSource" : {
      "Radius" : 2
    }
  },
  "views" : {
    "RenderView" : {
      "Background" : [0.0, 0.0, 0.0]
    }
  }
}
\end{python}

Note the hierarchical organization of the file. The first level of the
hierarchy specifies the group to which the object whose settings are
being specified refers (``sources'' in this example). The second level
names the object whose settings are being specified. Finally, the
third level specifies the custom default settings themselves. Note
that default values can be set to literal numbers, strings, or arrays
(denoted by comma-separated literals in square brackets).

The names of groups and objects come from the XML proxy definition
files in \ParaView's source code in the directory
\directory{ParaView/ParaViewCore/ServerManager/SMApplication/Resources}. The
group name is defined by the \texttt{name} attribute in a
\texttt{ProxyGroup} element. The object name comes from the
\texttt{name} attribute in the \texttt{Proxy} element (or elements of
\texttt{vtkSMProxy} subclasses). The property names come from the
\texttt{name} attribute in the \texttt{*Property} XML elements for the
object.

\begin{didyouknow}
The application-wide settings available in \paraview
through the \menu{Edit > Settings} menu are also saved to this user
settings file. Hence, if you have changed the application settings,
you will see some entries under a group named ``settings''.
\end{didyouknow}

\subsection{Configuring site-wide default settings}
\label{sec:ConfiguringDefaultSettingsSiteWide}

In addition to individual custom default settings, \ParaView offers a
way to specify site-wide custom default settings for a \ParaView
installation. These site-wide custom defaults must be defined in a
\JSON file with the same structure as the user settings file. In fact,
one way to create a site settings file is to set the custom defaults
desired in \paraview, close the program, and then copy the
user settings file to the site settings file. The site settings file
must be named \directory{ParaView-SiteSettings.json}.

The \ParaView executables will search for the site settings file in
several locations. If you installed \ParaView in the directory
\texttt{INSTALL}, then the \ParaView executables will search for the
site settings file in these directories in the specified order:

\begin{itemize}
\item \directory{INSTALL/share/paraview-X.Y}
\item \directory{INSTALL/lib}
\item \directory{INSTALL}
\item \directory{INSTALL/..}
\end{itemize}

where \texttt{X} is \ParaView's major version number and \texttt{Y} is
the minor version number. \ParaView executables will search these
directories in the given order, reading in the first
\directory{ParaView-SiteSettings.json} file it finds. The conventional
location for this kind of configuration file is in the \directory{share}
directory (the first directory searched), so we recommend placing the
site settings file there.

Custom defaults in the user settings file take precedence over custom
defaults in the site settings. If the same default is specified in
both the \directory{ParaView-SiteSettings.json} file and
\directory{ParaView-UserSettings.json} file in a user's directory, the
default specified in the \directory{ParaView-UserSettings.json} file will
be used. This is true for both object property settings and
application-settings set through the \menu{Edit > Settings} menu.

To aid in debugging problems with the site settings file location, you
can define an evironment variable named \texttt{PV\_SETTINGS\_DEBUG}
to something other than an empty string. This will turn on verbose
output showing where the \ParaView executables are looking for the site
settings file.
