\section{Tiled display}

\ParaView's parallel architecture makes it possible to visualize massive amounts
of data interactively. When the data is of sufficient resolution that parallel
processing is necessary for interactive display, it is often the case that
high-resolution images are needed to inspect the data in adequate detail. If you
have a 2D grid of display devices, you can run \ParaView in tiled display mode to
take advantage of it.

To put \ParaView in tiled display mode, give \pvserver (or \pvrenderserver) the X
and Y dimensions of the 2D grid with the \emph{--tile-dimensions-x} (or
\emph{-tdx}) and \emph{--tile-dimensions-y} (or \emph{‑tdy}) arguments. The X
and Y dimensions
default to 0, which disables tiled display mode. If you set only one of them to
a positive value on the command line, the other will be set to 1. In tiled
display mode, there must be at least as many server (in client / server mode) or
render server (in client / data server / render server mode) nodes as tiles. The
example below will create a $3\times 2$ tiled display.

\begin{shell}
pvserver -tdx=3 -tdy=2
\end{shell}

Unless you have a high-end display wall, it is likely that each monitor's bezel
creates a gap between the images shown in your tiled display. You can compensate
for the bezel width by considering them to be like mullions on a window. To do
that, specify the size of the gap (in pixels) through the \emph{--tile-mullion-x}
(\emph{-tmx}) and \emph{--tile-mullion-y} (\emph{-tmy}) command line arguments.

The IceT library, which \ParaView uses for its image compositing, has custom
compositing algorithms that work on tiled displays. Although compositing images
for large tiled displays is a compute-intensive process, IceT reduces the
overall amount of work by employing custom compositing strategies and removing
empty tiles from the computation, as demonstrated in
Figure~\ref{fig:TileComposite}. If the number
of nodes is greater than the number of tiles, then the image compositing work
will be divided amongst all the processes in the render server. In general,
rendering to a tiled display will perform significantly better if there are many
more nodes in the cluster than tiles in the display it drives. It also greatly
helps if the geometry to be rendered is spatially distributed. Spatially
distributed data is broken into contiguous pieces that are contained in small
regions of space and are therefore rendered to smaller areas of the screen. IceT
takes advantage of this property to reduce the amount of work required to
composite the final images. \ParaView includes the D3 filter, which redistributes
data amongst the processors to ensure a spatially distributed geometry and thus
improves tiled rendering performance.

[[File:.png|thumb|center|800px|'''Figure 13.5''' ]]
\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.9\linewidth]{Images/ParaView_UsersGuide_TileComposite.png}
\caption{Compositing images for 8 processes on a 4-tile display}
\label{fig:TileComposite}
\end{center}
\end{figure}

Unlike other parallel rendering modes, composited images are not delivered to
the client. Instead, image compositing is reserved for generating images on the
tiled display, and the desktop renders its own images from a lower resolution
version of the geometry to display in the UI. In Tiled Display Mode, \ParaView
automatically decimates the geometry and sends it to the client to make this
happen. However, when the data is very large, even a decimated version of the
geometry can overwhelm the client. In this case, \ParaView will replace the
geometry on the client with a bounding box.

You have several controls at run-time over the tiled rendering algorithm that
you can tune to maintain interactivity while visualizing very large data on very
high-resolution tiled displays. These are located on the Tile Display Parameters
section of the Render View / Server page of the application settings dialog.
These controls are described in the
\todo{FixReferences}
%[[ParaView/Users_Guide/Settings | Application Settings]]
section of the Appendix.
