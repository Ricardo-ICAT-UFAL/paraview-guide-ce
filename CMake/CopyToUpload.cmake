function(copy_to_upload source destination)
  set(upload_file "${CMAKE_CURRENT_BINARY_DIR}/${destination}")
  add_custom_target(upload_pdf_${source} DEPENDS ${upload_file})
  add_dependencies(upload_pdf_${source} pdf)
  add_dependencies(upload_pdf upload_pdf_${source})
  add_custom_command(
    OUTPUT ${upload_file}
    COMMAND ${CMAKE_COMMAND}
            -E copy ${CMAKE_CURRENT_BINARY_DIR}/${source} ${upload_file}
    DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${source})
endfunction()
